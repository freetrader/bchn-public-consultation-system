Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Optimizing code to be the fastest, most lightweight option available, making it more appealing for mining pools and leading to no brainer adoption across the board

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. Some sort of improvement/replacement for DAA, perhaps EMA as discussed by jtoomim here: https://www.youtube.com/watch?v=Fd6GFpZjLxU

Section C - Information about yourself
--------------------------------------

You would describe yourself as (you can check multiple responses):

- [X] - a Bitcoin Cash miner or mining executive
- [X] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [X] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [X] - a BCH user
- [ ] - other - please specify: ....................................
