Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. reduce minimum tx fee to 1 satoshi/transaction from 1 satoshi/byte (effectively, zero fees).
2. show list of sponsorships/donator on https://bitcoincashnode.org/ (include link-backs) to disclose potential conflicts of interest.
3. have all contributors/developers write a conflict of interest statement similar to the one written by Bob Summerwill https://bobsummerwill.com/conflict-of-interests-statement/ , and post it on bitcoincashnode.org

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. Implement changes to create a more diverse ecosystem for BCH miners (presently, 97% of BCH mining occurs on mining nodes controlled by companies HQ'd in China)
2. Increase p2p trading, reduce reliance on centralized third party exchanges.
3. Engage with top 10 USA miners and onboard their power provider onto a payment rail that allows them to pay for their power in BCH (BitPay most likely service provider).

Section C - Information about yourself
--------------------------------------

You would describe yourself as (you can check multiple responses):

- [x] - a Bitcoin Cash miner or mining executive
- [x] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [x] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [x] - other - please specify: executive of a company specializing in providing management and monitoring software for Bitcoin Cash miners.
