# Licensing applicable of various parts of BCHN PCS

## Licensing of input / output data and PCS documentation

All files in following subdirectories of this repository are licensed
under CC-BY-SA 4.0 (see CC-BY-SA-4.0.txt for the full license conditions):

- input/
- output/
- doc/

## Licensing of program source code

All files under the following subdirectories are licensed under
Affero GPL version 3 (AGPLv3) ( see AGPLv3.txt for full license conditions):

- src/own/

All files under the following subdirectories are licensed under the specific
license(s) provided by their contributors:

- src/contrib/

## Licensing of any other files

All other files in this repository are licensed under CC-BY-SA 4.0 unless
otherwise indicated in this file.
